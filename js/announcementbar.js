(function ($, Drupal) {
  Drupal.behaviors.announcementbar = {
    attach: function (context, settings) {

      var interval = drupalSettings.announcementbar.interval;
      var period = drupalSettings.announcementbar.period;
      console.log(interval);
      console.log(period);

      var expiresValue = function getCookiesTime(interval,period) {
        var date = new Date();
        switch(period) {
          case 'Minutes':
            date.setTime(
              date.getTime() + interval * 60 * 1000
            );
            return date;
              break;
          case 'Hours':
            date.setTime(
              date.getTime() + interval * 60 * 60 * 1000
            );
            return date;
              break;
          case 'Days':
            date.setTime(
              date.getTime() + interval * 24 * 60 * 60 * 1000
            );
            return date;
              break;
        }
      }

      const iCookieLength = 2; // Cookie length in mnt
      const sCookieName = 'announcement-bar';
      const sCookieContents = '1';

      function setCookie() {
        var expireDate = expiresValue(interval,period);
        document.cookie = `${sCookieName}=${sCookieContents}; expires=${expireDate.toGMTString()}; path=/;`;
      }

      function getCookie() {
        const cookieArray = document.cookie.split(';');
        for (let i = 0; i < cookieArray.length; i++) {
          let cookie = cookieArray[i];
          // Remove space between cookies.
          while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1);
          }
          // Cookie found? Return the cookie value.
          if (cookie.indexOf(sCookieName) === 0) {
            return cookie.substring(sCookieName.length, cookie.length);
          }
        }
        return null; // No cookie found.
      }

      const announcementBarCookie = getCookie();
      if (!announcementBarCookie) {
        $('.announcement-bar-wrapper').show();
      }
      else {
        $('.announcement-bar-wrapper').hide();
      }

      $('#announcement-bar-btn',context).click(function(){
        setCookie();
        $('.announcement-bar-wrapper').slideToggle('slow');
      });


    }
  }
})(jQuery, Drupal);


