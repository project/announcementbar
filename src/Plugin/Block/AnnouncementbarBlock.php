<?php
namespace Drupal\announcementbar\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Announcement Bar' block.
 *
 * @Block(
 *  id = "announcementbar",
 *  admin_label = @Translation("Announcementbar"),
 * )
*/

class AnnouncementbarBlock extends BlockBase {

 /**
  * {@inheritdoc}
  */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['announcementbar'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Announcementbar Settings Form'),
    ];
    $form['announcementbar']['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#default_value' => isset($config['message']) ? $config['message'] : '',
      '#required' => TRUE,
    ];
    $form['announcementbar']['button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CTA Button'),
      '#default_value' => isset($config['button']) ? $config['button'] : '',
      '#required' => TRUE,
    ];
    $form['announcementbar']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Position'),
      '#options' => array_combine(['top','bottom'], ['Top','Bottom']),
      '#default_value' => isset($config['position']) ? $config['position'] : '',
      '#empty_option' => '-None-',
    ];
    $form['announcementbar']['background'] = [
      '#type' => 'color',
      '#title' => $this->t('Background'),
      '#default_value' => isset($config['background']) ? $config['background'] : '',
    ];
    $form['announcementbar']['color'] = [
      '#type' => 'color',
      '#title' => 'Message Color',
      '#default_value' => isset($config['color']) ? $config['color'] : '',
    ];

    $form['announcementbar']['fieldset'] = [
      '#type' => 'fieldset',
      '#title' => 'Cookie Interval',
      '#attributes' => ['class' => ['container-inline'],'style'=>'border: none; padding: 20px 0 0 0;'],
    ];
    $form['announcementbar']['fieldset']['interval'] = [
       '#type' => 'number',
       '#default_value' => isset($config['interval']) ? $config['interval'] : '',
       '#required' => TRUE,
    ];
    $day = ['Minutes','Hours','Days'];
    $form['announcementbar']['fieldset']['period'] = [
      '#type' => 'select',
      '#options' => array_combine($day,$day),
      '#default_value' => isset($config['period']) ? $config['period'] : '',
    ];

    return $form;
  }

 /**
  * {@inheritdoc}
  */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $this->setConfigurationValue('message', $form_state->getValue(['announcementbar','message']));
    $this->setConfigurationValue('button', $form_state->getValue(['announcementbar','button']));
    $this->setConfigurationValue('position', $form_state->getValue(['announcementbar','position']));
    $this->setConfigurationValue('background', $form_state->getValue(['announcementbar','background']));
    $this->setConfigurationValue('color', $form_state->getValue(['announcementbar','color']));
    $this->setConfigurationValue('interval', $form_state->getValue(['announcementbar','fieldset','interval']));
    $this->setConfigurationValue('period', $form_state->getValue(['announcementbar','fieldset','period']));
  }

 /**
  * {@inheritdoc}
  */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#theme' => 'announcementbar_template',
      '#message' => isset($config['message']) ? $config['message'] : '',
      '#button' => isset($config['button']) ? $config['button'] : '',
      '#position' => isset($config['position']) ? $config['position'] : '',
      '#background' => isset($config['background']) ? $config['background'] : '',
      '#color' => isset($config['color']) ? $config['color'] : '',
      '#interval' => isset($config['interval']) ? $config['interval'] : '',
      '#period' => isset($config['period']) ? $config['period'] : '',
    ];
  }

}
