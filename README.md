# Notifybar

## Table of contents

- Introduction
- Installation
- Requirements
- Configuration
- Maintainers


## Introduction

Announcement bar module provide you functionality to show the notification bar as per block placement.
The block with name "Announcement Bar" to show on the site.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Requirements

This module requires the following modules:

- [Block](https://www.drupal.org/project/block)


## Configuration

1. Go to Home » Administration » Structure » Block layout» Add Announcement Bar Block
2. Add your message, button text, button link, background colour and text colour.
3. You can use the block("Announcementbar") to show in the specific region


## Maintainers:

- [Rajesh Bhimani](https://www.drupal.org/u/rajesh-bhimani)

Supporting organizations:

* [Skynet Technologies USA LLC](https://www.skynettechnologies.com)
